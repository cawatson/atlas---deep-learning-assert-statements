testCreateYieldsEqualsInstances_01,getN4JSCore,getClassifierID,createScript,createExportDeclaration,createExportSpecifier,createImportDeclaration,createNamedImportSpecifier,createDefaultImportSpecifier,createNamespaceImportSpecifier,createAnnotationList,createExpressionAnnotationList,createAnnotation,createLiteralAnnotationArgument,createTypeRefAnnotationArgument,createFunctionDeclaration,createFunctionExpression,createArrowFunction,createLocalArgumentsVariable,createFormalParameter,createBlock,createStatement,createVariableStatement,createExportedVariableStatement,createVariableBinding,createExportedVariableBinding,createVariableDeclaration,createExportedVariableDeclaration,createEmptyStatement,createExpressionStatement,createIfStatement,createIterationStatement,createDoStatement,createWhileStatement,createForStatement,createContinueStatement,createBreakStatement,createReturnStatement,createWithStatement,createSwitchStatement,createCaseClause,createDefaultClause,createLabelledStatement,createThrowStatement,createTryStatement,createCatchBlock,createCatchVariable,createFinallyBlock,createDebuggerStatement,createPrimaryExpression,createParenExpression,createIdentifierRef,createSuperLiteral,createThisLiteral,createArrayLiteral,createArrayElement,createArrayPadding,createObjectLiteral,createLiteralOrComputedPropertyName,createPropertyAssignmentAnnotationList,createPropertyNameValuePair,createPropertyNameValuePairSingleName,createPropertyMethodDeclaration,createPropertyGetterDeclaration,createPropertySetterDeclaration,createNewTarget,createNewExpression,createParameterizedCallExpression,createArgument,createIndexedAccessExpression,createTaggedTemplateString,createParameterizedPropertyAccessExpression,createAwaitExpression,createPromisifyExpression,createYieldExpression,createLiteral,createNullLiteral,createBooleanLiteral,createStringLiteral,createTemplateLiteral,createTemplateSegment,createNumericLiteral,createDoubleLiteral,createAbstractIntLiteral,createIntLiteral,createBinaryIntLiteral,createOctalIntLiteral,createLegacyOctalIntLiteral,createHexIntLiteral,createScientificIntLiteral,createRegularExpressionLiteral,createPostfixExpression,createUnaryExpression,createCastExpression,createMultiplicativeExpression,createAdditiveExpression,createShiftExpression,createRelationalExpression,createEqualityExpression,createBinaryBitwiseExpression,createBinaryLogicalExpression,createConditionalExpression,createAssignmentExpression,createCommaExpression,createN4ClassDeclaration,createN4ClassExpression,createN4InterfaceDeclaration,createN4EnumDeclaration,createN4EnumLiteral,createN4MemberAnnotationList,createN4FieldDeclaration,createN4MethodDeclaration,createN4GetterDeclaration,createN4SetterDeclaration,createBindingPattern,createObjectBindingPattern,createArrayBindingPattern,createBindingProperty,createBindingElement,
METHOD_0,METHOD_1,METHOD_2,METHOD_3,METHOD_4,METHOD_5,METHOD_6,METHOD_7,METHOD_8,METHOD_9,METHOD_10,METHOD_11,METHOD_12,METHOD_13,METHOD_14,METHOD_15,METHOD_16,METHOD_17,METHOD_18,METHOD_19,METHOD_20,METHOD_21,METHOD_22,METHOD_23,METHOD_24,METHOD_25,METHOD_26,METHOD_27,METHOD_28,METHOD_29,METHOD_30,METHOD_31,METHOD_32,METHOD_33,METHOD_34,METHOD_35,METHOD_36,METHOD_37,METHOD_38,METHOD_39,METHOD_40,METHOD_41,METHOD_42,METHOD_43,METHOD_44,METHOD_45,METHOD_46,METHOD_47,METHOD_48,METHOD_49,METHOD_50,METHOD_51,METHOD_52,METHOD_53,METHOD_54,METHOD_55,METHOD_56,METHOD_57,METHOD_58,METHOD_59,METHOD_60,METHOD_61,METHOD_62,METHOD_63,METHOD_64,METHOD_65,METHOD_66,METHOD_67,METHOD_68,METHOD_69,METHOD_70,METHOD_71,METHOD_72,METHOD_73,METHOD_74,METHOD_75,METHOD_76,METHOD_77,METHOD_78,METHOD_79,METHOD_80,METHOD_81,METHOD_82,METHOD_83,METHOD_84,METHOD_85,METHOD_86,METHOD_87,METHOD_88,METHOD_89,METHOD_90,METHOD_91,METHOD_92,METHOD_93,METHOD_94,METHOD_95,METHOD_96,METHOD_97,METHOD_98,METHOD_99,METHOD_100,METHOD_101,METHOD_102,METHOD_103,METHOD_104,METHOD_105,METHOD_106,METHOD_107,METHOD_108,METHOD_109,METHOD_110,METHOD_111,METHOD_112,METHOD_113,METHOD_114,METHOD_115,METHOD_116,METHOD_117,METHOD_118,

numberfour,n4js,projectModel,IN4JSProject,myProjectURI,second,emf,ecore,EClass,eClass,N4JSPackage,SCRIPT,EXPORT_DECLARATION,EXPORT_SPECIFIER,IMPORT_DECLARATION,NAMED_IMPORT_SPECIFIER,DEFAULT_IMPORT_SPECIFIER,NAMESPACE_IMPORT_SPECIFIER,ANNOTATION_LIST,EXPRESSION_ANNOTATION_LIST,ANNOTATION,LITERAL_ANNOTATION_ARGUMENT,TYPE_REF_ANNOTATION_ARGUMENT,FUNCTION_DECLARATION,FUNCTION_EXPRESSION,ARROW_FUNCTION,LOCAL_ARGUMENTS_VARIABLE,FORMAL_PARAMETER,BLOCK,STATEMENT,VARIABLE_STATEMENT,EXPORTED_VARIABLE_STATEMENT,VARIABLE_BINDING,EXPORTED_VARIABLE_BINDING,VARIABLE_DECLARATION,EXPORTED_VARIABLE_DECLARATION,EMPTY_STATEMENT,EXPRESSION_STATEMENT,IF_STATEMENT,ITERATION_STATEMENT,DO_STATEMENT,WHILE_STATEMENT,FOR_STATEMENT,CONTINUE_STATEMENT,BREAK_STATEMENT,RETURN_STATEMENT,WITH_STATEMENT,SWITCH_STATEMENT,CASE_CLAUSE,DEFAULT_CLAUSE,LABELLED_STATEMENT,THROW_STATEMENT,TRY_STATEMENT,CATCH_BLOCK,CATCH_VARIABLE,FINALLY_BLOCK,DEBUGGER_STATEMENT,PRIMARY_EXPRESSION,PAREN_EXPRESSION,IDENTIFIER_REF,SUPER_LITERAL,THIS_LITERAL,ARRAY_LITERAL,ARRAY_ELEMENT,ARRAY_PADDING,OBJECT_LITERAL,LITERAL_OR_COMPUTED_PROPERTY_NAME,PROPERTY_ASSIGNMENT_ANNOTATION_LIST,PROPERTY_NAME_VALUE_PAIR,PROPERTY_NAME_VALUE_PAIR_SINGLE_NAME,PROPERTY_METHOD_DECLARATION,PROPERTY_GETTER_DECLARATION,PROPERTY_SETTER_DECLARATION,NEW_TARGET,NEW_EXPRESSION,PARAMETERIZED_CALL_EXPRESSION,ARGUMENT,INDEXED_ACCESS_EXPRESSION,TAGGED_TEMPLATE_STRING,PARAMETERIZED_PROPERTY_ACCESS_EXPRESSION,AWAIT_EXPRESSION,PROMISIFY_EXPRESSION,YIELD_EXPRESSION,LITERAL,NULL_LITERAL,BOOLEAN_LITERAL,STRING_LITERAL,TEMPLATE_LITERAL,TEMPLATE_SEGMENT,NUMERIC_LITERAL,DOUBLE_LITERAL,ABSTRACT_INT_LITERAL,INT_LITERAL,BINARY_INT_LITERAL,OCTAL_INT_LITERAL,LEGACY_OCTAL_INT_LITERAL,HEX_INT_LITERAL,SCIENTIFIC_INT_LITERAL,REGULAR_EXPRESSION_LITERAL,POSTFIX_EXPRESSION,UNARY_EXPRESSION,CAST_EXPRESSION,MULTIPLICATIVE_EXPRESSION,ADDITIVE_EXPRESSION,SHIFT_EXPRESSION,RELATIONAL_EXPRESSION,EQUALITY_EXPRESSION,BINARY_BITWISE_EXPRESSION,BINARY_LOGICAL_EXPRESSION,CONDITIONAL_EXPRESSION,ASSIGNMENT_EXPRESSION,COMMA_EXPRESSION,N4_CLASS_DECLARATION,N4_CLASS_EXPRESSION,N4_INTERFACE_DECLARATION,N4_ENUM_DECLARATION,N4_ENUM_LITERAL,N4_MEMBER_ANNOTATION_LIST,N4_FIELD_DECLARATION,N4_METHOD_DECLARATION,N4_GETTER_DECLARATION,N4_SETTER_DECLARATION,BINDING_PATTERN,OBJECT_BINDING_PATTERN,ARRAY_BINDING_PATTERN,BINDING_PROPERTY,BINDING_ELEMENT,
IDENT_0,IDENT_1,IDENT_2,IDENT_3,IDENT_4,IDENT_5,IDENT_6,IDENT_7,IDENT_8,IDENT_9,IDENT_10,IDENT_11,IDENT_12,IDENT_13,IDENT_14,IDENT_15,IDENT_16,IDENT_17,IDENT_18,IDENT_19,IDENT_20,IDENT_21,IDENT_22,IDENT_23,IDENT_24,IDENT_25,IDENT_26,IDENT_27,IDENT_28,IDENT_29,IDENT_30,IDENT_31,IDENT_32,IDENT_33,IDENT_34,IDENT_35,IDENT_36,IDENT_37,IDENT_38,IDENT_39,IDENT_40,IDENT_41,IDENT_42,IDENT_43,IDENT_44,IDENT_45,IDENT_46,IDENT_47,IDENT_48,IDENT_49,IDENT_50,IDENT_51,IDENT_52,IDENT_53,IDENT_54,IDENT_55,IDENT_56,IDENT_57,IDENT_58,IDENT_59,IDENT_60,IDENT_61,IDENT_62,IDENT_63,IDENT_64,IDENT_65,IDENT_66,IDENT_67,IDENT_68,IDENT_69,IDENT_70,IDENT_71,IDENT_72,IDENT_73,IDENT_74,IDENT_75,IDENT_76,IDENT_77,IDENT_78,IDENT_79,IDENT_80,IDENT_81,IDENT_82,IDENT_83,IDENT_84,IDENT_85,IDENT_86,IDENT_87,IDENT_88,IDENT_89,IDENT_90,IDENT_91,IDENT_92,IDENT_93,IDENT_94,IDENT_95,IDENT_96,IDENT_97,IDENT_98,IDENT_99,IDENT_100,IDENT_101,IDENT_102,IDENT_103,IDENT_104,IDENT_105,IDENT_106,IDENT_107,IDENT_108,IDENT_109,IDENT_110,IDENT_111,IDENT_112,IDENT_113,IDENT_114,IDENT_115,IDENT_116,IDENT_117,IDENT_118,IDENT_119,IDENT_120,IDENT_121,IDENT_122,IDENT_123,IDENT_124,IDENT_125,IDENT_126,







"The class '","' is not a valid classifier",
STRING_0,STRING_1,


























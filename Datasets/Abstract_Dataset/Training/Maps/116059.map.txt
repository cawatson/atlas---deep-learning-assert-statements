calculateColdEmissionsAndThrowEventTest_Exceptions,setUp,getFactory,createVehicle,createVehicleType,calculateColdEmissionsAndThrowEvent,getNonScenarioVehicles,isUsingVehicleTypeIdAsVehicleDescription,getDescription,setDescription,convertVehicleDescription2VehicleInformationTuple,getFirst,getColdPollutantDoubleMap,rescaleColdEmissions,ColdEmissionEvent,processEvent,
METHOD_0,METHOD_1,METHOD_2,METHOD_3,METHOD_4,METHOD_5,METHOD_6,METHOD_7,METHOD_8,METHOD_9,METHOD_10,METHOD_11,METHOD_12,METHOD_13,METHOD_14,METHOD_15,

matsim,contrib,emissions,v01,Id,vehicles,VehicleType,testCasesExceptions,excep,vehicleInfoForNoCase,vehicleTypeId,Link,linkId,Vehicle,vehicleId,vehicle,VehicleUtils,coldEmissionAnalysisModule,startTime,TestColdEmissionAnalysisModule,parkingDuration,tableAccDistance,ecg,NonScenarioVehicles,abort,EmissionsConfigGroup,GROUP_NAME,ignore,noVehWarnCnt,ColdEmissionAnalysisModule,Gbl,FUTURE_SUPPRESSED,EmissionSpecificationMarker,BEGIN_EMISSIONS,END_EMISSIONS,vehicleDescription,Tuple,HbefaVehicleCategory,HbefaVehicleAttributes,vehicleInformationTuple,EmissionUtils,coldEmissions,distance_km,emissionEfficiencyFactor,Event,coldEmissionEvent,eventTime,coldEmissionEventLinkId,eventsManager,
IDENT_0,IDENT_1,IDENT_2,IDENT_3,IDENT_4,IDENT_5,IDENT_6,IDENT_7,IDENT_8,IDENT_9,IDENT_10,IDENT_11,IDENT_12,IDENT_13,IDENT_14,IDENT_15,IDENT_16,IDENT_17,IDENT_18,IDENT_19,IDENT_20,IDENT_21,IDENT_22,IDENT_23,IDENT_24,IDENT_25,IDENT_26,IDENT_27,IDENT_28,IDENT_29,IDENT_30,IDENT_31,IDENT_32,IDENT_33,IDENT_34,IDENT_35,IDENT_36,IDENT_37,IDENT_38,IDENT_39,IDENT_40,IDENT_41,IDENT_42,IDENT_43,IDENT_44,IDENT_45,IDENT_46,IDENT_47,IDENT_48,







"PASSENGER_CAR;PC diesel;;>=2L",";;;","'"," was used to calculate cold emissions and generate an emissions event.","It should instead throw an exception because it is not a valid vehicle information string.","linkId","vehicleId","Vehicle is null. ","Please make sure that requirements for emission vehicles in "," config group are met."," Or set the parameter + 'nonScenarioVehicles' to 'ignore' in order to skip such vehicles."," Aborting...","Vehicle will be ignored.","Not yet implemented. Aborting..."," config group are met. Aborting...","Vehicle category for vehicle "," is not valid. ",
STRING_0,STRING_1,STRING_2,STRING_3,STRING_4,STRING_5,STRING_6,STRING_7,STRING_8,STRING_9,STRING_10,STRING_11,STRING_12,STRING_13,STRING_14,STRING_15,STRING_16,

























